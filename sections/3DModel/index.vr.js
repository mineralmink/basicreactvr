import React, { Component } from 'react';

import { AppRegistry, asset, Pano, View, Text, Model } from 'react-vr';

export default class Basics extends Component {
  render(){
    return(
      <View>
        <Pano source={asset('maxresdefault.jpg')}></Pano>
        <Model
          source={{obj:asset('knight.obj')}}
          style={{transform: [{translate: [0, -1, -3]}]}}
          texture={asset('armor.jpg')}
        />
      </View>
    )
  }
};

AppRegistry.registerComponent('Basics', () => Basics);
